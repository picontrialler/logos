#!/bin/bash

srindex=$1
lamedbdir=$2
saveto=$3

cat $srindex | grep -v '#' | grep -v '\-\-\-' | grep -v '^$' | while read line ; do
    serviceref=`echo $line | tr [A-Z] [a-z]`
    serviceref=(${serviceref//_/ })
    lamedbref=`echo ${serviceref[0]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`":"`echo ${serviceref[3]} | sed -e :a -e 's/^.\{1,7\}$/0&/;ta'`":"`echo ${serviceref[1]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`":"`echo ${serviceref[2]} | sed -e :a -e 's/^.\{1,3\}$/0&/;ta'`

    if [ -z "$(cat $lamedbdir/* | grep $lamedbref)" ]; then
        echo $line >> $saveto
    fi
done

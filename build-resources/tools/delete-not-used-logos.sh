#!/bin/bash

srindex=$1
location=$2

cat $srindex | grep -e '^# .*' -e '\-\-\- NO DATA' | while read line ; do
    if echo "$line" | grep -q '^# '; then
        logoname=${line#??}
    fi

    if echo "$line" | grep -q '\-\-\- NO DATA'; then
        for file in $(find $location -type f -name "$logoname.png"); do
            rm $file
        done
    fi
done

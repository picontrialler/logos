#!/bin/bash

#sudo apt-get install p7zip-full imagemagick pngnq librsvg2-bin binutils

version=$(date +"%Y-%m-%d--%H-%M-%S")

if [ "$1" != "build" ] && [ "$1" != "todo" ] && [ "$1" != "status" ]; then
    echo ""
    echo "Please run the script like this:"
    echo ""
    echo "./build.sh build     (This starts the actual building of the picons)"
    echo "./build.sh todo      (Generates a todo list to help fix mistakes)"
    echo "./build.sh status    (Generates a status file of the channels and their link to a picon)"
    echo ""
    exit 1
fi

REPODIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SOURCEDIR=$REPODIR/build-source
LAMEDBDIR=$REPODIR/build-resources/lamedb-output
LAMEDBDIRINPUT=$REPODIR/build-resources/lamedb-input
TOOLSDIR=$REPODIR/build-resources/tools

TEMP=/tmp/picons-tmp
TEMPBINARIES=/tmp/picons-binaries
TEMPPICONS=$TEMP/picons
TEMPSOURCEPICONS=$TEMP/sourcepicons
TEMPSOURCEPICONSBLACK=$TEMPSOURCEPICONS/black
TEMPSOURCEPICONSWHITE=$TEMPSOURCEPICONS/white
TEMPSYMLINKS=$TEMP/symlinks

LOGFILE=/tmp/picons.log
echo $version > $LOGFILE

chmod -R 755 $TOOLSDIR/*.sh

# Generate status.txt
if [ "$1" = "status" ]; then
    $TOOLSDIR/generate-status.sh $LAMEDBDIR $SOURCEDIR /tmp/status.txt $LAMEDBDIRINPUT
    echo "Status file has been saved in /tmp/status.txt"
fi

# Generate todo.txt
if [ "$1" = "todo" ]; then
    echo "Busy generating a todo list..."
    echo -e "Generated on:\n$(date +"%Y-%m-%d %H:%M:%S")\n\n" > /tmp/todo.txt

    for file in $(find $SOURCEDIR -path ./.git -prune -o -path ./backgrounds -prune -o -type f -name '*.svg'); do
        if [ -f ${file%.*}.png ]; then
            rm ${file%.*}.png
        fi
    done

    cat $SOURCEDIR/tv.srindex | sed -e 's/.*/\U&\E/' | sed -e 's/^# .*/\L&\E/' | sed 's/[ \t]*$//' > $SOURCEDIR/tv.srindex_temp ; mv $SOURCEDIR/tv.srindex_temp $SOURCEDIR/tv.srindex
    cat $SOURCEDIR/radio.srindex | sed -e 's/.*/\U&\E/' | sed -e 's/^# .*/\L&\E/' | sed 's/[ \t]*$//' > $SOURCEDIR/radio.srindex_temp ; mv $SOURCEDIR/radio.srindex_temp $SOURCEDIR/radio.srindex

    positions=`cat $SOURCEDIR/*.srindex | grep '^\-' | tr [a-z] [A-Z] | sort | uniq -c`
    echo "Orbital index:" >> /tmp/todo.txt
    echo "$positions" >> /tmp/todo.txt
    echo "" >> /tmp/todo.txt

    duplicates=`cat $SOURCEDIR/*.srindex | grep '.*_.*_.*_.*' | tr [a-z] [A-Z] | sort | uniq -d`
    if [ ! -z "$duplicates" ]; then
        echo "The following serviceref duplicates were found:" >> /tmp/todo.txt
        echo "$duplicates" >> /tmp/todo.txt
        echo "" >> /tmp/todo.txt
    fi

    duplicates=`cat $SOURCEDIR/tv.srindex | grep '^# .*' | tr [A-Z] [a-z] | sort | uniq -d`
    if [ ! -z "$duplicates" ]; then
        echo "The following logo duplicates were found in tv.srindex:" >> /tmp/todo.txt
        echo "$duplicates" >> /tmp/todo.txt
        echo "" >> /tmp/todo.txt
    fi

    duplicates=`cat $SOURCEDIR/radio.srindex | grep '^# .*' | tr [A-Z] [a-z] | sort | uniq -d`
    if [ ! -z "$duplicates" ]; then
        echo "The following logo duplicates were found in radio.srindex:" >> /tmp/todo.txt
        echo "$duplicates" >> /tmp/todo.txt
        echo "" >> /tmp/todo.txt
    fi

    echo "The following servicerefs are not found in lamedb for tv.srindex:" >> /tmp/todo.txt
    $TOOLSDIR/check-servicerefs.sh $SOURCEDIR/tv.srindex $LAMEDBDIR /tmp/todo.txt
    echo "" >> /tmp/todo.txt
    echo "The following servicerefs are not found in lamedb for radio.srindex:" >> /tmp/todo.txt
    $TOOLSDIR/check-servicerefs.sh $SOURCEDIR/radio.srindex $LAMEDBDIR /tmp/todo.txt
    echo "" >> /tmp/todo.txt

    $TOOLSDIR/check-images.sh $SOURCEDIR/tv.srindex $SOURCEDIR/tv /tmp/todo.txt
    $TOOLSDIR/check-images.sh $SOURCEDIR/radio.srindex $SOURCEDIR/radio /tmp/todo.txt

    echo "Todo list has been saved in /tmp/todo.txt"
fi

# Generate picons
if [ "$1" = "build" ]; then

    if [ -d "$TEMP" ]; then
        rm -rf $TEMP
    fi
    mkdir $TEMP

    if ! [ -d "$TEMPBINARIES" ]; then
        mkdir $TEMPBINARIES
    else
        rm -rf $TEMPBINARIES
        mkdir $TEMPBINARIES
    fi

    echo $(date +"%H:%M:%S") - Creating symlinks
    $TOOLSDIR/create-symlinks.sh $SOURCEDIR/tv.srindex $TEMPSYMLINKS/tv $LAMEDBDIR
    $TOOLSDIR/create-symlinks.sh $SOURCEDIR/radio.srindex $TEMPSYMLINKS/radio $LAMEDBDIR

    echo $(date +"%H:%M:%S") - Converting svg files
    mkdir -p $TEMPSOURCEPICONSBLACK/tv
    mkdir -p $TEMPSOURCEPICONSBLACK/radio
    mkdir -p $TEMPSOURCEPICONSWHITE/tv
    mkdir -p $TEMPSOURCEPICONSWHITE/radio

    cp $SOURCEDIR/tv/*.* $TEMPSOURCEPICONSBLACK/tv
    cp $SOURCEDIR/radio/*.* $TEMPSOURCEPICONSBLACK/radio

    for file in $(find $TEMPSOURCEPICONS -type f -name '*.svg'); do
       #inkscape -z -T -w 300 -e ${file%.*}.png $file >> $LOGFILE 2>&1
       rsvg-convert -w 300 -h 300 -a -f png -o ${file%.*}.png $file
       rm $file
    done

    cp $TEMPSOURCEPICONSBLACK/tv/*.* $TEMPSOURCEPICONSWHITE/tv
    cp $TEMPSOURCEPICONSBLACK/radio/*.* $TEMPSOURCEPICONSWHITE/radio
    cp $SOURCEDIR/tv/white/*.* $TEMPSOURCEPICONSWHITE/tv
    cp $SOURCEDIR/radio/white/*.* $TEMPSOURCEPICONSWHITE/radio

    for file in $(find $TEMPSOURCEPICONS -type f -name '*.svg'); do
        #inkscape -z -T -w 300 -e ${file%.*}.png $file >> $LOGFILE 2>&1
        rsvg-convert -w 300 -h 300 -a -f png -o ${file%.*}.png $file
        rm $file
    done

    echo $(date +"%H:%M:%S") - Deleting unused logos
    $TOOLSDIR/delete-not-used-logos.sh $SOURCEDIR/tv.srindex $TEMPSOURCEPICONSBLACK/tv
    $TOOLSDIR/delete-not-used-logos.sh $SOURCEDIR/tv.srindex $TEMPSOURCEPICONSWHITE/tv
    $TOOLSDIR/delete-not-used-logos.sh $SOURCEDIR/radio.srindex $TEMPSOURCEPICONSBLACK/radio
    $TOOLSDIR/delete-not-used-logos.sh $SOURCEDIR/radio.srindex $TEMPSOURCEPICONSWHITE/radio

    for background in $SOURCEDIR/backgrounds/*.txt ; do

        backgroundname=${background%.*}
        backgroundname=${backgroundname##*/}

        for backgroundcolor in $SOURCEDIR/backgrounds/$backgroundname/*.png ; do

            backgroundcolor=${backgroundcolor%.*}.png

            backgroundcolorname=${backgroundcolor%.*}
            backgroundcolorname=${backgroundcolorname##*/}

            echo $(date +"%H:%M:%S") -----------------------------------------------------------
            echo $(date +"%H:%M:%S") - Creating picons: $backgroundname.$backgroundcolorname

            if [[ $backgroundcolorname == *-white* ]]; then
                USETEMPSOURCEPICONS=$TEMPSOURCEPICONSWHITE
            else
                USETEMPSOURCEPICONS=$TEMPSOURCEPICONSBLACK
            fi

            mkdir -p $TEMPPICONS/picon

            for directory in $USETEMPSOURCEPICONS/* ; do
                if [ -d $directory ]; then
                    directory=${directory##*/}
                    for logo in $USETEMPSOURCEPICONS/$directory/*.png ; do
                        logoname=${logo##*/}
                        logoname=${logoname%.*}
                        firstchar=`echo $logoname | grep -o '^.'`
                        fullfilepath=$TEMPPICONS/picon/$directory/$firstchar/$logoname.png

                        if ! [ -d $TEMPPICONS/picon/$directory/$firstchar ]; then
                            mkdir -p $TEMPPICONS/picon/$directory/$firstchar
                        fi

                        case $backgroundname in
                            "sd")
                                if [[ $backgroundcolorname == *-nopadding ]]; then resize="70x53"; else resize="62x45"; fi
                                extent="70x53"
                                compress="pngnq -g 2.2 -s 1"
                                ;;
                            "hd")
                                if [[ $backgroundcolorname == *-nopadding ]]; then resize="100x60"; else resize="86x46"; fi
                                extent="100x60"
                                compress="pngnq -g 2.2 -s 1"
                                ;;
                            "shd")
                                if [[ $backgroundcolorname == *-nopadding ]]; then resize="220x132"; else resize="189x101"; fi
                                extent="220x132"
                                compress="pngnq -g 2.2 -s 1"
                                ;;
                            "xvdr")
                                if [[ $backgroundcolorname == *-nopadding ]]; then resize="256x256"; else resize="226x226"; fi
                                extent="256x256"
                                compress="cat"
                                ;;
                        esac

                        convert $backgroundcolor \( $logo -background none -bordercolor none -border 100 -trim -resize $resize -gravity center -extent $extent +repage \) -layers merge - 2>> $LOGFILE | $compress > $fullfilepath 2>> $LOGFILE

                    done
                fi
            done

            if [ "$backgroundname" = "hd" ] || [ "$backgroundname" = "shd" ] || [ "$backgroundname" = "sd" ]; then

                echo $(date +"%H:%M:%S") - Copying symlinks: $backgroundname.$backgroundcolorname
                cp -P $TEMPSYMLINKS/tv/1_* $TEMPPICONS/picon
                cp -P $TEMPSYMLINKS/radio/1_* $TEMPPICONS/picon

                echo $(date +"%H:%M:%S") - Creating ipk: $backgroundname.$backgroundcolorname
                mkdir $TEMPPICONS/CONTROL
                echo "Package: enigma2-plugin-picons-tv-ocram.$backgroundname.$backgroundcolorname" > $TEMPPICONS/CONTROL/control
                echo "Version: $version" >> $TEMPPICONS/CONTROL/control
                echo "Section: base" >> $TEMPPICONS/CONTROL/control
                echo "Architecture: all" >> $TEMPPICONS/CONTROL/control
                echo "Maintainer: http://picons.bitbucket.org" >> $TEMPPICONS/CONTROL/control
                echo "Source: https://bitbucket.org/picons/logos/src" >> $TEMPPICONS/CONTROL/control
                echo "Description: $backgroundname Picons ($backgroundcolorname)" >> $TEMPPICONS/CONTROL/control
                echo "OE: enigma2-plugin-picons-tv-ocram.$backgroundname.$backgroundcolorname" >> $TEMPPICONS/CONTROL/control
                echo "HomePage: http://picons.bitbucket.org" >> $TEMPPICONS/CONTROL/control
                echo "License: unknown" >> $TEMPPICONS/CONTROL/control
                echo "Priority: optional" >> $TEMPPICONS/CONTROL/control
                chmod -R 777 $TEMPPICONS
                $TOOLSDIR/ipkg-build.sh -o root -g root $TEMPPICONS $TEMPBINARIES >> $LOGFILE

                echo $(date +"%H:%M:%S") - Creating tar.bz2: $backgroundname.$backgroundcolorname
                mkdir $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                cp -P -r $TEMPPICONS/picon/* $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                chmod -R 777 $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                tar --owner=root --group=root -cjf $TEMPBINARIES/$backgroundname.$backgroundcolorname\_$version.tar.bz2 -C $TEMPPICONS $backgroundname.$backgroundcolorname\_$version
                rm -rf $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version

                echo $(date +"%H:%M:%S") - Creating 7z: $backgroundname.$backgroundcolorname
                mkdir $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                cp -H $TEMPPICONS/picon/1_*.png $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                chmod -R 777 $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                7z a -t7z -mx9 $TEMPBINARIES/$backgroundname.$backgroundcolorname\_$version.7z $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version >> $LOGFILE

            fi

            if [ "$backgroundname" = "xvdr" ]; then

                echo $(date +"%H:%M:%S") - Copying symlinks: $backgroundname.$backgroundcolorname
                cp -P $TEMPSYMLINKS/tv/1_* $TEMPPICONS/picon
                cp -P $TEMPSYMLINKS/radio/1_* $TEMPPICONS/picon

                echo $(date +"%H:%M:%S") - Creating tar.bz2: $backgroundname.$backgroundcolorname
                mkdir $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                cp -P -r $TEMPPICONS/picon/* $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                chmod -R 777 $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                tar --owner=root --group=root -cjf $TEMPBINARIES/$backgroundname.$backgroundcolorname\_$version.tar.bz2 -C $TEMPPICONS $backgroundname.$backgroundcolorname\_$version
                rm -rf $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version

                echo $(date +"%H:%M:%S") - Creating 7z: $backgroundname.$backgroundcolorname
                mkdir $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                cp -H $TEMPPICONS/picon/1_*.png $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                chmod -R 777 $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version
                7z a -t7z -mx9 $TEMPBINARIES/$backgroundname.$backgroundcolorname\_$version.7z $TEMPPICONS/$backgroundname.$backgroundcolorname\_$version >> $LOGFILE

            fi

            rm -rf $TEMPPICONS

        done

    done

    touchstamp=`echo ${version//-/} | rev | cut -c 3- | rev`.`echo ${version//-/} | cut -c 13-`
    for file in $TEMPBINARIES/* ; do
        touch -t $touchstamp "$file"
    done

    if [ -d "$TEMP" ]; then
        rm -rf $TEMP
    fi

fi
